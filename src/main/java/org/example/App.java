package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    private static final Logger LOGGER = LogManager.getLogger(App.class);

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        final int initialPoolSize = 10;
        final int maximalPoolSize = 100;
        final long databaseConnectionIdleTimeoutMillis = 10000;//10 sekund

        ConnectionPool connectionPool =
                new ConnectionPool(initialPoolSize, maximalPoolSize, databaseConnectionIdleTimeoutMillis);

        int numberOfThreads = 100000;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);

        connectionPool.scheduler();

        for (int i = 0; i < numberOfThreads; i++) {
            final int iteration = i;
            executorService.submit(() -> handleConnection(connectionPool, iteration));
        }

        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(1, TimeUnit.MINUTES)) {
                LOGGER.warn("Executor service did not terminate in the specified time. Forcing shutdown...");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.error("Await termination was interrupted. Forcing shutdown now...", e);
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        LOGGER.info("Executor service successfully shut down.");

        try {
            connectionPool.shutdownScheduler();
            LOGGER.info("Connection pool scheduler successfully shut down.");
        } catch (Exception e) {
            LOGGER.error("Failed to shut down connection pool scheduler.", e);
        }
        connectionPool.showSize();
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        long minutes = (duration / 1000) / 60;
        long seconds = (duration / 1000) % 60;

        LOGGER.info("Total time taken: {} minute(s), {} seconds", minutes, seconds);
    }

    private static void handleConnection(ConnectionPool connectionPool, int iteration) {
        ConnectionWrapper connectionWrapper = null;
        try {
            connectionWrapper = connectionPool.getConnectionWrapper();
            if (iteration % 3 == 0) {
                falseQueryHandle(connectionWrapper);
            } else {
                correctQueryHandle(connectionWrapper);
            }
            Thread.sleep(20000); // 20 sekund
        } catch (NoAvailableConnectionException e) {
            LOGGER.error("No available connection: {}", e.getMessage());
            throw new RuntimeException("No available connection", e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.warn("Thread interrupted: {}", e.getMessage());
            throw new RuntimeException("Thread was interrupted", e);
        } catch (NewConnectionFailureException e) {
            LOGGER.error("Connection pool exception: {}", e.getMessage());
            throw new RuntimeException("Error in connection pool", e);
        } finally {
            if (connectionWrapper != null) {
                connectionPool.releaseConnection(connectionWrapper);
            }
        }
    }

    public static void correctQueryHandle(ConnectionWrapper connectionWrapper) {
        if (connectionWrapper == null) {
            LOGGER.error("Connection is null, cannot proceed with user processing");
            throw new IllegalArgumentException("Connection is null, cannot proceed with user processing");
        }
        try (Statement statement = connectionWrapper.getConnection().createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT id, username, password FROM users")) {
            LOGGER.info("Started processing users.");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String username = resultSet.getString("username");
                String pass = resultSet.getString("password");
                LOGGER.info("ID: {}, Username: {}, Password: {}", id, username, pass);
            }
            LOGGER.info("Finished processing users successfully.");
        } catch (SQLException e) {
            LOGGER.error("An error occurred while processing users: {}", e.getMessage(), e);
        }
    }

    public static void falseQueryHandle(ConnectionWrapper connectionWrapper) {
        if (connectionWrapper == null) {
            LOGGER.error("Connection is null, cannot proceed with user processing");
            throw new IllegalArgumentException("Connection is null, cannot proceed with user processing");
        }
        try (Statement statement = connectionWrapper.getConnection().createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT id, user, pass FROM users")) { // Błędne zapytanie
            LOGGER.info("Started processing users.");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String username = resultSet.getString("user");
                String pass = resultSet.getString("pass");
                LOGGER.info("ID: {}, Username: {}, Password: {}", id, username, pass);
            }
            LOGGER.info("Finished processing users successfully.");
        } catch (SQLException e) {
            LOGGER.error("An error occurred while processing users: {}", e.getMessage(), e);
        }
    }

}