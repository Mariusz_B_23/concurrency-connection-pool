package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {

    private static final Logger LOGGER = LogManager.getLogger(ConfigLoader.class);
    private static final Properties properties = new Properties();
    private static final String CONFIG_FILE = "dbconfig.properties";


    static {
        try (InputStream input = ConfigLoader.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
            if (input == null) {
                LOGGER.error("Unable to find " + CONFIG_FILE);
            } else {
                properties.load(input);
            }
        } catch (IOException ex) {
            LOGGER.error("Error reading the database configuration file: {}", ex.getMessage(), ex);
        }
    }

    private ConfigLoader() {
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static String getUrl() {
        return getProperty("db.url");
    }

    public static String getUser() {
        return getProperty("db.user");
    }

    public static String getPassword() {
        return getProperty("db.password");
    }

}