package org.example;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private final int initialPoolSize;
    private final int maximalPoolSize;
    private final long databaseConnectionIdleTimeoutMillis;

    @Getter
    private final List<ConnectionWrapper> mainConnectionPool;

    private final ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);


    public ConnectionPool(int initialPoolSize, int maximalPoolSize, long databaseConnectionIdleTimeoutMillis) {
        this.initialPoolSize = initialPoolSize;
        this.maximalPoolSize = maximalPoolSize;
        this.databaseConnectionIdleTimeoutMillis = databaseConnectionIdleTimeoutMillis;
        this.mainConnectionPool = new ArrayList<>(initialPoolSize);
        initializeConnections();
    }

    private void initializeConnections() {
        for (int i = 0; i < initialPoolSize; i++) {
            try {
                addNewConnection();
            } catch (Exception e) {
                LOGGER.error("Failed to add new connection during pool initialization", e);
            }
        }
        LOGGER.info("Initial pool size after initialization: {}", mainConnectionPool.size());
    }

    public ConnectionWrapper addNewConnection() throws NewConnectionFailureException {
        synchronized (mainConnectionPool) {
            if (isConnectionPoolFull()) {
                LOGGER.error("Add connection error. Connection pool is full: {}", mainConnectionPool.size());
                throw new IllegalStateException("Cannot add new connection, pool is already full");
            } else {
                try {
                    Connection connection = DatabaseConnection.connect();
                    ConnectionWrapper connectionWrapper = new ConnectionWrapper(connection);
                    mainConnectionPool.add(connectionWrapper);
                    LOGGER.info("New connection added: {}. Current pool size: {}",
                            connectionWrapper, mainConnectionPool.size());
                    return connectionWrapper;
                } catch (SQLException e) {
                    LOGGER.error("Failed to create a new connection", e);
                    throw new NewConnectionFailureException("Failed to create a new connection", e);
                }
            }
        }
    }

    private boolean isConnectionPoolFull() {
        synchronized (mainConnectionPool) {
            return mainConnectionPool.size() >= maximalPoolSize;
        }
    }

    public ConnectionWrapper getConnectionWrapper() throws NoAvailableConnectionException, NewConnectionFailureException {
        synchronized (mainConnectionPool) {
            for (ConnectionWrapper connectionWrapper : mainConnectionPool) {
                try {
                    if (!connectionWrapper.getConnection().isClosed() && !connectionWrapper.isInUse()) {
                        connectionWrapper.setInUse(true);
                        connectionWrapper.setLastUpdate(System.currentTimeMillis());
                        LOGGER.info("Returning existing connection: {}", connectionWrapper);
                        return connectionWrapper;
                    }
                } catch (SQLException e) {
                    LOGGER.error("Error checking connection: {}", e.getMessage());
                }
            }
            if (mainConnectionPool.size() < maximalPoolSize) {
                ConnectionWrapper newConnection = addNewConnection();
                newConnection.setInUse(true);
                newConnection.setLastUpdate(System.currentTimeMillis());
                LOGGER.info("Returning new connection: {}", newConnection);
                return newConnection;
            }
        }
        LOGGER.warn("No available connections and pool is at maximum capacity.");
        throw new NoAvailableConnectionException("No available connections and pool is at maximum capacity.");
    }

    public void releaseConnection(ConnectionWrapper connectionWrapper) {
        if (connectionWrapper == null) {
            throw new IllegalArgumentException("Attempted to release a null connection.");
        }
        synchronized (mainConnectionPool) {
            if (!connectionWrapper.isInUse()) {
                LOGGER.warn("Attempted to release a connection {} that is not in use.",
                        connectionWrapper.getId());
            } else {
                connectionWrapper.setInUse(false);
                connectionWrapper.setLastUpdate(System.currentTimeMillis());
                LOGGER.info("Connection released: {}", connectionWrapper);
            }
        }
    }

    public void scheduler() {
        scheduledExecutor.scheduleAtFixedRate(() -> {
            synchronized (mainConnectionPool) {
                LOGGER.info("SCHEDULER START.");
                Iterator<ConnectionWrapper> iterator = mainConnectionPool.iterator();
                while (iterator.hasNext()) {
                    ConnectionWrapper connectionWrapper = iterator.next();
                    if (!connectionWrapper.isInUse() &&
                            (System.currentTimeMillis() - connectionWrapper.getLastUpdate()) >
                                    databaseConnectionIdleTimeoutMillis) {
                        DatabaseConnection.close(connectionWrapper.getConnection());
                        iterator.remove();
                        showSize();
                    }
                }
                while (mainConnectionPool.size() < initialPoolSize) {
                    try {
                        addNewConnection();
                    } catch (NewConnectionFailureException e) {
                        LOGGER.error("Failed to add new connection during scheduler run: {}", e.getMessage());
                    }
                }
                LOGGER.info("SCHEDULER STOP.");
            }
        }, 1, 1, TimeUnit.MINUTES);
    }

    public void shutdownScheduler() {
        LOGGER.info("Shutting down scheduler...");
        scheduledExecutor.shutdown();
        try {
            if (!scheduledExecutor.awaitTermination(1, TimeUnit.MINUTES)) {
                LOGGER.warn("Forcing shutdown as tasks did not terminate in the given time...");
                scheduledExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.error("Shutdown was interrupted, forcing shutdown...");
            scheduledExecutor.shutdownNow();
            Thread.currentThread().interrupt();
        }
        if (scheduledExecutor.isTerminated()) {
            LOGGER.info("Scheduler successfully shut down.");
        } else {
            LOGGER.warn("Scheduler did not shut down completely.");
        }
    }

    public void showSize() {
        LOGGER.info("CONNECTION POOL SIZE: {}", mainConnectionPool.size());
    }

    public boolean isSchedulerShutdown() {
        return scheduledExecutor.isShutdown();
    }

}