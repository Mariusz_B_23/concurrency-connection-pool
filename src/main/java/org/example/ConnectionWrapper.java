package org.example;

import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.concurrent.atomic.AtomicInteger;


@Getter
public class ConnectionWrapper {

    private final Connection connection;
    @Getter
    @Setter
    private boolean inUse;
    @Setter
    private long lastUpdate;

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger();
    private final int id;


    public ConnectionWrapper(Connection connection) {
        this.connection = connection;
        this.inUse = false;
        this.lastUpdate = System.currentTimeMillis();
        this.id = ID_GENERATOR.getAndIncrement() + 1;
    }

    @Override
    public String toString() {
        return "ConnectionWrapper{" +
                "connection = " + System.identityHashCode(connection) +
                ", inUse = " + inUse +
                ", setLastUpdate = " + lastUpdate +
                ", id = " + id +
                '}';
    }
}