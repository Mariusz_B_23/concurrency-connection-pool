package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    private static final Logger LOGGER = LogManager.getLogger(DatabaseConnection.class);

    private static final String URL = ConfigLoader.getUrl();
    private static final String USER = ConfigLoader.getUser();
    private static final String PASSWORD = ConfigLoader.getPassword();


    private DatabaseConnection() {
    }

    public static Connection connect() throws SQLException {
        Connection connection;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            LOGGER.info("Connected to the database.");
        } catch (SQLException e) {
            LOGGER.error("Error connecting to the database: {}", e.getMessage(), e);
            throw e;
        }
        return connection;
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                LOGGER.info("Connection closed.");
            } catch (SQLException e) {
                LOGGER.error("Error closing the connection: {}", e.getMessage(), e);
            }
        }
    }

}