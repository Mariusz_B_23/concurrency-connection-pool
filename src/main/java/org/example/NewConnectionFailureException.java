package org.example;

public class NewConnectionFailureException extends Exception {

    public NewConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}