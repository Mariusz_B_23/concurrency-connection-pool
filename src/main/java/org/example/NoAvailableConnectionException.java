package org.example;

public class NoAvailableConnectionException extends Exception {

    public NoAvailableConnectionException(String message) {
        super(message);
    }

}