import org.example.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ConnectionPoolTest {

    private ConnectionPool connectionPool;
    private ConnectionWrapper connectionWrapperMock;
    private MockedStatic<DatabaseConnection> databaseConnectionMockedStatic;

    @BeforeEach
    void setUp() throws SQLException {
        Connection connectionMock = mock(Connection.class);
        connectionWrapperMock = mock(ConnectionWrapper.class);
        when(connectionMock.isClosed()).thenReturn(false);

        if (databaseConnectionMockedStatic != null) {
            databaseConnectionMockedStatic.close();
        }

        databaseConnectionMockedStatic = Mockito.mockStatic(DatabaseConnection.class);
        databaseConnectionMockedStatic.when(DatabaseConnection::connect).thenReturn(connectionMock);

        connectionPool = new ConnectionPool(5, 10, 60000);
    }

    @AfterEach
    void tearDown() {
        if (databaseConnectionMockedStatic != null) {
            databaseConnectionMockedStatic.close();
        }
        if (connectionPool != null) {
            connectionPool.shutdownScheduler();
        }
    }

    @Test
    void testInitialPoolSize() {
        assertEquals(5, connectionPool.getMainConnectionPool().size(),
                "Initial pool size should be 5");
    }

    @Test
    void testAddNewConnectionWhenNotFull() throws Exception {
        connectionPool.addNewConnection();
        assertEquals(6, connectionPool.getMainConnectionPool().size(),
                "Pool size should increase after adding a new connection");
    }

    @Test
    void testAddNewConnectionWhenFull() throws NewConnectionFailureException {
        for (int i = 0; i < 5; i++) {
            connectionPool.addNewConnection();
        }
        assertThrows(IllegalStateException.class, () -> connectionPool.addNewConnection(),
                "Adding a connection to a full pool should throw an exception");
    }


    @Test
    void testGetConnectionWrapperExisting() throws Exception {
        ConnectionWrapper connectionWrapper = connectionPool.getConnectionWrapper();
        assertNotNull(connectionWrapper, "getConnectionWrapper should return an available connection");
        assertTrue(connectionWrapper.isInUse(), "Connection should be marked as in use");
    }

    @Test
    void testGetConnectionWrapperWhenPoolFull() throws Exception {
        for (int i = 0; i < 10; i++) {
            connectionPool.getConnectionWrapper();
        }
        assertThrows(NoAvailableConnectionException.class, () -> connectionPool.getConnectionWrapper(),
                "Should throw exception when no connections are available");
    }

    @Test
    void testReleaseConnection() {
        when(connectionWrapperMock.isInUse()).thenReturn(true);
        connectionPool.releaseConnection(connectionWrapperMock);
        verify(connectionWrapperMock).setInUse(false);
    }

    @Test
    void testSchedulerRemovesIdleConnections() {
        connectionPool.scheduler();
        assertTrue(connectionPool.getMainConnectionPool().size() >= 5,
                "Scheduler should maintain minimum pool size");
    }

    @Test
    void testShutdownScheduler() {
        connectionPool.shutdownScheduler();
        assertTrue(connectionPool.isSchedulerShutdown(), "Scheduler should be shut down");
    }
}
